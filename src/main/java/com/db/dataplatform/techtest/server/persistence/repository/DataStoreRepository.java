package com.db.dataplatform.techtest.server.persistence.repository;

import com.db.dataplatform.techtest.server.persistence.BlockTypeEnum;
import com.db.dataplatform.techtest.server.persistence.model.DataBodyEntity;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DataStoreRepository extends JpaRepository<DataBodyEntity, Long> {
	
	@Query("select entity from DataBodyEntity entity where entity.dataHeaderEntity.blockType =:blockType")
	public List<DataBodyEntity> findAllByBlockType(@Param("blockType") BlockTypeEnum blockType);
	
	@Query("select entity from DataBodyEntity entity where entity.dataHeaderEntity.name =:blockName")
	public Optional<DataBodyEntity> findByBlockName(@Param("blockName") String blockName);

}
