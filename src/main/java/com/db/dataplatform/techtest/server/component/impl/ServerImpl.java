package com.db.dataplatform.techtest.server.component.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.db.dataplatform.techtest.Constant;
import com.db.dataplatform.techtest.server.api.model.DataBody;
import com.db.dataplatform.techtest.server.api.model.DataEnvelope;
import com.db.dataplatform.techtest.server.api.model.DataHeader;
import com.db.dataplatform.techtest.server.component.Server;
import com.db.dataplatform.techtest.server.exception.HadoopClientException;
import com.db.dataplatform.techtest.server.persistence.BlockTypeEnum;
import com.db.dataplatform.techtest.server.persistence.model.DataBodyEntity;
import com.db.dataplatform.techtest.server.persistence.model.DataHeaderEntity;
import com.db.dataplatform.techtest.server.service.DataBodyService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class ServerImpl implements Server {

	private final DataBodyService dataBodyServiceImpl;
	private final ModelMapper modelMapper;
	private final RestTemplate restTemplate;
	
	public static final String URI_HADOOP_DATALAKE_PUSHDATA = "http://localhost:8090/hadoopserver/pushbigdata";
	
	

	/**
	 * @param envelope
	 * @return true if there is a match with the client provided checksum.
	 */
	@Override
	public boolean saveDataEnvelope(DataEnvelope envelope) {

		DataBody body = envelope.getDataBody();
		DataHeader header = envelope.getDataHeader();
		String checkSum = null;

		try {
			checkSum = calculateMD5CheckSum(body.getDataBody().getBytes());
		} catch (NoSuchAlgorithmException e) {
			log.error("Exception occurred calculating :{}",e.getMessage());
		} catch (IOException e) {
			log.error("Exception occurred calculating :{}",e.getMessage());
		}
		if (!header.getCheckSum().equals(checkSum)) {
			return false;
		}
		// Save to persistence.
		persist(envelope, checkSum);
		log.info("Data persisted successfully, data name: {}", envelope.getDataHeader().getName());

		return true;
	}
	
	@Async
	public boolean pushToHadoop(DataEnvelope envelope) {
		// Push data to Bank's Hadoop data lake
		try {
			pushToHadoopDataLake(envelope);
			return true;
		} catch (HadoopClientException e) {
			log.error("Exception occurred when processing Hadoop data lake : {}", e.getMessage());
		} catch (InterruptedException e) {
			log.error("Interruption excepiton occurred when processing Hadoop data lake : {}", e.getMessage());
		}
		return false;
	}
	
	
	
	/**
	 * Push Data to Bank's Hadoop data lake
	 * @param envelope
	 * @throws HadoopClientException
	 * @throws InterruptedException
	 */
	@SuppressWarnings("unchecked")
	private void pushToHadoopDataLake(DataEnvelope envelope) throws HadoopClientException,InterruptedException {
		ObjectMapper mapper = new ObjectMapper();
		try {
			String payload  = mapper.writeValueAsString(envelope);
			ResponseEntity<HttpStatus> status = restTemplate.postForEntity(URI_HADOOP_DATALAKE_PUSHDATA, payload, HttpStatus.class);
			if(status.getStatusCode() == HttpStatus.GATEWAY_TIMEOUT) {
				throw new HadoopClientException("Hadoop Request Timed out");
			}
		} catch (JsonProcessingException e) {
			log.error("Exception occurred when processing Object to Json");
		} 
		
	}

	private void persist(DataEnvelope envelope, String checkSum) {
		log.info("Persisting data with attribute name: {}", envelope.getDataHeader().getName());
		DataHeaderEntity dataHeaderEntity = modelMapper.map(envelope.getDataHeader(), DataHeaderEntity.class);
		dataHeaderEntity.setCheckSum(checkSum);

		DataBodyEntity dataBodyEntity = modelMapper.map(envelope.getDataBody(), DataBodyEntity.class);
		dataBodyEntity.setDataHeaderEntity(dataHeaderEntity);

		saveData(dataBodyEntity);
	}

	private void saveData(DataBodyEntity dataBodyEntity) {
		dataBodyServiceImpl.saveDataBody(dataBodyEntity);
	}

	/**
	 * Calculate content MD5 checksum.
	 */
	private String calculateMD5CheckSum(byte[] fis) throws IOException, NoSuchAlgorithmException {

		DigestInputStream dis = new DigestInputStream(new ByteArrayInputStream(fis),
				MessageDigest.getInstance(Constant.MD5));

		byte[] buffer = new byte[8192];
		while (dis.read(buffer) > 0);
		String md5Content = new String(
				org.apache.commons.codec.binary.Base64.encodeBase64(dis.getMessageDigest().digest()));
		return md5Content;
	}

	@Override
	public List<DataEnvelope> getDataEnvelopeForBlockType(String blockType) {
		List<DataEnvelope> envelopeList = new ArrayList<>();
		List<DataBodyEntity> dataBodyEntityList = dataBodyServiceImpl
				.getDataByBlockType(BlockTypeEnum.getBlockTypeEnum(blockType));
		for (DataBodyEntity entity : dataBodyEntityList) {
			DataHeader header = modelMapper.map(entity.getDataHeaderEntity(), DataHeader.class);
			DataBody body = modelMapper.map(entity, DataBody.class);
			envelopeList.add(new DataEnvelope(header, body));
		}
		return envelopeList;
	}

	@Override
	public boolean savePatchData(String blockName, String newBlockType) {

		BlockTypeEnum blockTypeEnum = BlockTypeEnum.getBlockTypeEnum(newBlockType);
		if (blockTypeEnum == null) {
			return false;
		}
		try {
			Optional<DataBodyEntity> dataBodyEntity = dataBodyServiceImpl.getDataByBlockName(blockName);
			DataBodyEntity entity = dataBodyEntity.get();
			if (entity != null) {
				DataHeaderEntity headerEntity = entity.getDataHeaderEntity();
				headerEntity.setBlocktype(blockTypeEnum);
				entity.setDataHeaderEntity(headerEntity);
				saveData(entity);
				return true;
			}
			return false;
		}catch(NoSuchElementException exception) {
			log.info(exception.getMessage());
			return false;
		}
	}

}

