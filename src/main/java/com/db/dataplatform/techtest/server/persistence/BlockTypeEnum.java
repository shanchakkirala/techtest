package com.db.dataplatform.techtest.server.persistence;

public enum BlockTypeEnum {
    BLOCKTYPEA("blocktypea"),
    BLOCKTYPEB("blocktypeb");

    private final String type;

    BlockTypeEnum(String type) {
        this.type = type;
    }
    
    public static BlockTypeEnum getBlockTypeEnum(String value) {
      for(BlockTypeEnum e : BlockTypeEnum.values()) {
    	 if(e.getType().equals(value)) {
    	      return e;
         }
      }
      return null;
    }

	public String getType() {
		return type;
	}

}
