package com.db.dataplatform.techtest.client.component.impl;

import com.db.dataplatform.techtest.Constant;
import com.db.dataplatform.techtest.client.api.model.DataEnvelope;
import com.db.dataplatform.techtest.client.component.Client;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Client code does not require any test coverage
 */

@Service
@Slf4j
@RequiredArgsConstructor
public class ClientImpl implements Client {
	

	private final RestTemplate restTemplate;

    public static final String URI_PUSHDATA = "http://localhost:8090/dataserver/pushdata";
    public static final UriTemplate URI_GETDATA = new UriTemplate("http://localhost:8090/dataserver/data/{blockType}");
    public static final UriTemplate URI_PATCHDATA = new UriTemplate("http://localhost:8090/dataserver/update/{name}/{newBlockType}");
    public static final String GET_PARAMETER_KEY_BLOCKTYPE ="GET_PARAMETER_KEY_BLOCKTYPE";
    public static final String PATCH_PARAMETER_KEY_BLOCKTYPE ="PATCH_PARAMETER_KEY_BLOCKTYPE";
    public static final String PATCH_PARAMETER_KEY_BLOCKNAME ="PATCH_PARAMETER_KEY_BLOCKNAME";
    
    @Override
    public void pushData(DataEnvelope dataEnvelope) {
    	
    	log.info("Pushing data {} to {}", dataEnvelope.getDataHeader().getName(), URI_PUSHDATA);
    	ResponseEntity<Boolean> result = restTemplate.postForEntity(URI_PUSHDATA, dataEnvelope, Boolean.class);
    	if(result != null) {
    		log.info("Push data successful");
    	} else {
    		log.info("Push data failed");
    	}
        
    }

    @Override
    public List<DataEnvelope> getData(String blockType) {
    	log.info("Query for data with header block type {}", blockType);
//    	Map<String, String> uriVariables = new HashMap<>();
//    	uriVariables.put(GET_PARAMETER_KEY_BLOCKTYPE, blockType);  	

        @SuppressWarnings("unchecked")
		List<DataEnvelope> list =  restTemplate.getForObject(URI_GETDATA.toString(), List.class,blockType);        
        return list;
    }
    

    @Override
    public boolean updateData(String blockName, String newBlockType) {
        log.info("Updating blocktype to {} for block with name {}", newBlockType, blockName);
//        Map<String, String> uriVariables = new HashMap<>();
//        uriVariables.put(PATCH_PARAMETER_KEY_BLOCKTYPE,newBlockType);
//        uriVariables.put(PATCH_PARAMETER_KEY_BLOCKNAME,blockName);
        return restTemplate.patchForObject(URI_PATCHDATA.toString(), blockName, Boolean.class, blockName, newBlockType);
    }
      


}
