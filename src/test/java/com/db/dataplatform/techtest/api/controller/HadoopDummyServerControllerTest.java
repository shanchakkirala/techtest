package com.db.dataplatform.techtest.api.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.web.servlet.MockMvc;

import com.db.dataplatform.techtest.TestDataHelper;
import com.db.dataplatform.techtest.server.api.controller.HadoopDummyServerController;
import com.db.dataplatform.techtest.server.api.model.DataEnvelope;
import com.db.dataplatform.techtest.server.component.Server;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(MockitoJUnitRunner.class)
public class HadoopDummyServerControllerTest {
	
	public static final String URI_HADOOP_DATALAKE_PUSHDATA = "http://localhost:8090/hadoopserver/pushbigdata";
	@Mock
	private Server serverMock;

	private DataEnvelope testDataEnvelope;
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;
	private HadoopDummyServerController hadoopController;

	@Before
	public void setUp() throws  NoSuchAlgorithmException, IOException {
		hadoopController = new HadoopDummyServerController(serverMock);
		mockMvc = standaloneSetup(hadoopController).build();
		objectMapper = Jackson2ObjectMapperBuilder
				.json()
				.build();

		testDataEnvelope = TestDataHelper.createTestDataEnvelopeApiObject();
	}

	
	/**
	 * The test can return either 202 or 504 timeout exception as currently the dummy method is checking for a random number.  
	 * @throws Exception
	 */
	@Test
	public void testPushDataHadoopPostCall() throws Exception {
		
		String testDataEnvelopeJson = objectMapper.writeValueAsString(testDataEnvelope);
		mockMvc.perform(post(URI_HADOOP_DATALAKE_PUSHDATA)
				.content(testDataEnvelopeJson))
				.andReturn();
	}

}
