package com.db.dataplatform.techtest.service;

import com.db.dataplatform.techtest.server.api.model.DataEnvelope;
import com.db.dataplatform.techtest.server.mapper.ServerMapperConfiguration;
import com.db.dataplatform.techtest.server.persistence.BlockTypeEnum;
import com.db.dataplatform.techtest.server.persistence.model.DataBodyEntity;
import com.db.dataplatform.techtest.server.persistence.model.DataHeaderEntity;
import com.db.dataplatform.techtest.server.service.DataBodyService;
import com.db.dataplatform.techtest.server.component.Server;
import com.db.dataplatform.techtest.server.component.impl.ServerImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import static com.db.dataplatform.techtest.TestDataHelper.createTestDataEnvelopeApiObject;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ServerServiceTests {

	@Mock
	private DataBodyService dataBodyServiceImplMock;

	private ModelMapper modelMapper;

	private DataBodyEntity expectedDataBodyEntity;
	private DataEnvelope testDataEnvelope;
	private RestTemplate restTemplate;

	private Server server;

	@Before
	public void setup() {
		ServerMapperConfiguration serverMapperConfiguration = new ServerMapperConfiguration();
		modelMapper = serverMapperConfiguration.createModelMapperBean();

		testDataEnvelope = createTestDataEnvelopeApiObject();
		expectedDataBodyEntity = modelMapper.map(testDataEnvelope.getDataBody(), DataBodyEntity.class);
		expectedDataBodyEntity
				.setDataHeaderEntity(modelMapper.map(testDataEnvelope.getDataHeader(), DataHeaderEntity.class));
		restTemplate = new RestTemplate();

		server = new ServerImpl(dataBodyServiceImplMock, modelMapper, restTemplate);
	}

	@Test
	public void shouldSaveDataEnvelopeAsExpected() throws NoSuchAlgorithmException, IOException {
		boolean success = server.saveDataEnvelope(testDataEnvelope);
		assertThat(success).isTrue();
	}

	@Test
	public void getData() throws NoSuchAlgorithmException, IOException {
		testDataEnvelope = createTestDataEnvelopeApiObject();
		String blockType = BlockTypeEnum.BLOCKTYPEA.getType();
		List<DataEnvelope> list = server.getDataEnvelopeForBlockType(blockType);
		assertEquals(0, list.size());
	}

	@Test
	public void savePatchData() {

		testDataEnvelope = createTestDataEnvelopeApiObject();
		String blockType = BlockTypeEnum.BLOCKTYPEB.getType();
		String blockName = "NewBlockName";
		boolean success = server.savePatchData(blockName, blockType);
		assertThat(success).isFalse();
	}
}
